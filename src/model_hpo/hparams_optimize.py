from nni.experiment import Experiment
from pathlib import Path
import json

class HParamsOptimization():

    def __init__(self, configuration_parameters: dict = None, experiment_parameters: dict = None) -> None:
        if configuration_parameters:
            self.configuration_parameters = configuration_parameters
        if experiment_parameters:
            self.experiment_parameters = experiment_parameters

    def load_config(self, configuration_parameters: dict = None, experiment_parameters: dict = None) -> None:
        if configuration_parameters:
            self.configuration_parameters = configuration_parameters
        if experiment_parameters:
            self.experiment_parameters = experiment_parameters

    def run_experiment(self):
        # Load experiment parameters
        experiment = Experiment(self.experiment_parameters['platform'])
        experiment.config.experiment_name = self.experiment_parameters['experiment_name']
        experiment.config.trial_concurrency = int(self.experiment_parameters['trial_concurrency'])
        experiment.config.max_trial_number = int(self.experiment_parameters['trial_concurrency'])
        experiment.config.tuner.name = self.experiment_parameters['tuner_name']
        experiment.config.tuner.class_args['optimize_mode'] = self.experiment_parameters['tuner_optimize_mode']
        experiment.config.training_service.use_active_gpu = self.experiment_parameters['use_active_gpu']
        experiment.config.search_space_file = self.config['search_space_file_path']
        
        # Load configuration parameters
        experiment.config.trial_command = 'python3 ' + self.configuration_parameters['model_execution_file_path']
        experiment.config.trial_code_directory = Path(self.configuration_parameters['model_execution_file_path']).parent
        
        success = experiment.run(8080, wait_completion = True)

        if not success:
            print("ERROR running experiment, check experiment logs at: nni/experiments/{}".format(experiment.config.experiment_name))