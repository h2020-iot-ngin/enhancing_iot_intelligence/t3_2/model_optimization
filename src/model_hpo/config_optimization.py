import random
import string

# get random string of letters and digits
source = string.ascii_letters + string.digits
random_len = 8

# ---------------- Configuration parameters ----------------
configuration_parameters = {   
    "search_space_file_path": './search_space.json',
    "model_execution_file_path": './model.py'
}
# ------------------------------------------------------------------------


# ---------------- NNI experiment configuration parameters ----------------
experiment_parameters = {
    # Platform to execute scripts 
    "platform": 'local', # local, remote, kubeflow
    # Name for NNI experiment
    "experiment_name": 'optimization_' + ''.join((random.choice(source) for i in range(random_len))),
    # Maximum number of trials executed concurrently
    "trial_concurrency": 2,
    # Maximum number of total trials to execute
    "max_trial_number": 10,
    # Name of parameter tuner algorithm
    "tuner_name": 'TPE', # TPE, Random, Anneal, Evolution, SMAC, GPTuner, MetisTuner, DNGOTuner, Hyperband, BOHB
    # Whether to maximize the parameters or minimize
    "tuner_optimize_mode": 'maximize', # maximize, minimize
    # Whether or not to use the GPU acceleration if available
    "use_active_gpu": True
}
# ------------------------------------------------------------------------