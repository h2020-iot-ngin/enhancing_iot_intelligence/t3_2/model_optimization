from enum import Enum
import os
from typing import Any, List

import torch

class PrunerType(Enum):
    # One-shot pruners
    LevelPruner = 'LevelPruner'
    SlimPruner = 'SlimPruner'
    FPGMPruner = 'FPGMPruner'
    L1FilterPruner = 'L1FilterPruner' 
    L2FilterPruner = 'L2FilterPruner'
    # Iterative pruners
    LotteryTicketPruner = 'LotteryTicketPruner'
    SimulatedAnnealingPruner = 'SimulatedAnnealingPruner'
    NetAdaptPruner = 'NetAdaptPruner'
    AutoCompressPruner = 'AutoCompressPruner'
    SensitivityPruner  = 'SensitivityPruner'
    AMCPruner = 'AMCPruner'

class PytorchQuantizerType(Enum):
    QAT_Quantizer = 'QAT_Quantizer'
    LsqQuantizer = 'LsqQuantizer'
    NaiveQuantizer = 'NaiveQuantizer'
    DoReFaQuantizer = 'DoReFaQuantizer' 

class TensorflowQuantizerType(Enum):
    DynamicRangeQuantizer = 'DynamicRangeQuantizer'
    FullIntegerQuantizer = 'FullIntegerQuantizer'
    Float16Quantizer = 'Float16Quantizer'

class Compressor():

    def __init__(self, backend_type: str = 'pytorch', export_path: str = None) -> None:
        if backend_type.lower() != 'pytorch' and backend_type.lower() != 'tensorflow':
            raise Exception("Backend {} not understood. Valid values are: 'pytorch' or 'tensorflow'.".format(backend_type))
        self.backend_type = backend_type.lower()

        if self.backend_type == 'pytorch':
            self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        self.export_path = export_path

    def prune(self, model: Any, config_list: List, pruner_type: str):
        if self.backend_type == 'pytorch':            
            # One-shot pruners (do not require training when pruning)
            if pruner_type.lower() == PrunerType.LevelPruner.value.lower():
                from nni.algorithms.compression.pytorch.pruning import LevelPruner
                pruner = LevelPruner(model, config_list)
            elif pruner_type.lower() == PrunerType.FPGMPruner.value.lower():
                from nni.algorithms.compression.pytorch.pruning import FPGMPruner
                pruner = FPGMPruner(model, config_list)
            elif pruner_type.lower() == PrunerType.L1FilterPruner.value.lower():
                from nni.algorithms.compression.pytorch.pruning import L1FilterPruner
                pruner = L1FilterPruner(model, config_list)
            elif pruner_type.lower() == PrunerType.L2FilterPruner.value.lower():
                from nni.algorithms.compression.pytorch.pruning import L2FilterPruner
                pruner = L2FilterPruner(model, config_list)
            # Iterative pruners (require training when pruning)
            elif pruner_type.lower() == PrunerType.SlimPruner.value.lower():
                from nni.algorithms.compression.pytorch.pruning import SlimPruner
                pruner = SlimPruner(model, config_list)
            elif pruner_type.lower() == PrunerType.LotteryTicketPruner.value.lower():
                from nni.algorithms.compression.pytorch.pruning import LotteryTicketPruner
                pruner = LotteryTicketPruner(model, config_list)
            elif pruner_type.lower() == PrunerType.SimulatedAnnealingPruner.value.lower():
                from nni.algorithms.compression.pytorch.pruning import SimulatedAnnealingPruner
                pruner = SimulatedAnnealingPruner(model, config_list)
            elif pruner_type.lower() == PrunerType.NetAdaptPruner.value.lower():
                from nni.algorithms.compression.pytorch.pruning import NetAdaptPruner 
                pruner = NetAdaptPruner(model, config_list)
            elif pruner_type.lower() == PrunerType.AutoCompressPruner.value.lower():
                from nni.algorithms.compression.pytorch.pruning import AutoCompressPruner
                pruner = AutoCompressPruner(model, config_list)
            elif pruner_type.lower() == PrunerType.SensitivityPruner.value.lower():
                from nni.algorithms.compression.pytorch.pruning import SensitivityPruner
                pruner = SensitivityPruner(model, config_list)
            elif pruner_type.lower() == PrunerType.AMCPruner.value.lower():
                from nni.algorithms.compression.pytorch.pruning import AMCPruner
                pruner = AMCPruner(model, config_list)
            else:
                raise Exception("Pruner {} not supported. Valid pruners for Pytorch are: {}.".format(pruner_type, [prun.value for prun in PrunerType])) 
            
            pruned_model = pruner.compress()
            if self.export_path is not None:
                pruner.export_model(os.path.join(self.export_path, 'pruned_model.pth'), os.path.join(self.export_path, 'pruned_model_masks.pth'), device=self.device)
            else:
                pruner.export_model('pruned_model.pth', 'pruned_model_masks.pth', device=self.device)
        else:
            from nni.algorithms.compression.tensorflow.pruning import LevelPruner, SlimPruner

            if pruner_type.lower() not in [PrunerType.LevelPruner.value.lower(), PrunerType.SlimPruner.value.lower()]:
                raise Exception("Pruner {} not supported. Valid pruners for Tensorflow are 'LevelPruner' or 'SlimPruner'.".format(pruner_type)) 
            # One-shot pruner (do not require training when pruning)
            elif pruner_type.lower() == PrunerType.LevelPruner.value.lower():
                pruner = LevelPruner(model, config_list)
            # Iterative pruner (require training when pruning)
            elif pruner_type.lower() == PrunerType.SlimPruner.value.lower():
                pruner = SlimPruner(model, config_list)

            pruned_model = pruner.compress()
            pruner.export_model('pruned_model.h5', 'pruned_model_masks', device=self.device)

        return pruned_model

    def speed_up(self, model: Any, masks_file_path: str, input_data: List = None, target_data: List = None):
        if self.backend_type == 'pytorch':
            from nni.compression.pytorch import ModelSpeedup

            m_speedup = ModelSpeedup(model, dummy_input=input_data[0], masks_file=masks_file_path)
            m_speedup.speedup_model()

            if input_data is not None and target_data is not None:
                print("Fine-tuning the model to recover accurazy with input and target data")
                # Fine-tune the model to recover accurazy
                optimizer = torch.optim.SGD(model.parameters(), lr=0.01)
                criterion = torch.nn.NLLLoss()
                epoch = 1
                tensor_x = torch.Tensor(input_data)
                tensor_y = torch.Tensor(target_data)
                torch_dataset = torch.utils.data.TensorDataset(tensor_x,tensor_y) # create your datset
                torch_loader = torch.utils.data.DataLoader(torch_dataset, batch_size=64)
                model.train()
                for batch_idx, (data, target) in enumerate(torch_loader):
                    data, target = data.to(self.device), target.to(self.device)
                    optimizer.zero_grad()
                    output = model(data)
                    loss = criterion(output, target)
                    loss.backward()
                    optimizer.step()
                    if batch_idx % 100 == 0:
                        print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                            epoch, batch_idx * len(data), len(torch_loader.dataset),
                            100. * batch_idx / len(torch_loader), loss.item()))

            return model
        else:
            raise Exception("Model speed up is not supported in tensorflow, only in pytorch") 

    def quantize(self, model: Any, config_list: List, quantizer_type: str, input_data: List = None, target_data: List = None):
        if self.backend_type == 'pytorch':
            if quantizer_type.lower() == PytorchQuantizerType.QAT_Quantizer.value.lower():
                from nni.algorithms.compression.pytorch.quantization import QAT_Quantizer
                quantizer = QAT_Quantizer(model, config_list)
            elif quantizer_type.lower() == PytorchQuantizerType.LsqQuantizer.value.lower():
                from nni.algorithms.compression.pytorch.quantization import LsqQuantizer
                quantizer = LsqQuantizer(model, config_list)
            elif quantizer_type.lower() == PytorchQuantizerType.NaiveQuantizer.value.lower():
                from nni.algorithms.compression.pytorch.quantization import NaiveQuantizer
                quantizer = NaiveQuantizer(model, config_list)
            elif quantizer_type.lower() == PytorchQuantizerType.DoReFaQuantizer.value.lower():
                from nni.algorithms.compression.pytorch.quantization import DoReFaQuantizer
                quantizer = DoReFaQuantizer(model, config_list)
            
            quantizer.compress()

            if input_data is not None and target_data is not None:
                print("Fine-tune the model to calibrate its weigths with input and target data")
                # Fine-tune the model to calibrate
                optimizer = torch.optim.SGD(model.parameters(), lr=0.01)
                criterion = torch.nn.NLLLoss()
                epoch = 1
                tensor_x = torch.Tensor(input_data)
                tensor_y = torch.Tensor(target_data)
                torch_dataset = torch.utils.data.TensorDataset(tensor_x,tensor_y)
                torch_loader = torch.utils.data.DataLoader(torch_dataset, batch_size=64)
                model.train()
                for batch_idx, (data, target) in enumerate(torch_loader):
                    data, target = data.to(self.device), target.to(self.device)
                    optimizer.zero_grad()
                    output = model(data)
                    loss = criterion(output, target)
                    loss.backward()
                    optimizer.step()
                    if batch_idx % 100 == 0:
                        print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                            epoch, batch_idx * len(data), len(torch_loader.dataset),
                            100. * batch_idx / len(torch_loader), loss.item()))
            
            if self.export_path is not None:
                quantizer.export_model(os.path.join(self.export_path, 'quantized_model.pth'), calibration_path=os.path.join(self.export_path, 'quantized_model_masks.pth'), device=self.device)
            else:
                quantizer.export_model('quantized_model.pth', calibration_path='quantized_model_masks.pth', device=self.device)
            
            return model

        else: 
            def representative_data_gen():
                for input_value in tf.data.Dataset.from_tensor_slices(input_data).batch(1).take(100):
                    # Model has only one input so each data point has one element.
                    yield [input_value]

            import tensorflow as tf
            if isinstance(model, str):
                converter = tf.lite.TFLiteConverter.from_saved_model(model)
            else:
                converter = tf.lite.TFLiteConverter.from_keras_model(model)
            
            if quantizer_type.lower() == TensorflowQuantizerType.DynamicRangeQuantizer.value.lower():
                converter.optimizations = [tf.lite.Optimize.DEFAULT]
                tflite_quant_model = converter.convert()
                
            elif quantizer_type.lower() == TensorflowQuantizerType.FullIntegerQuantizer.value.lower():
                converter.optimizations = [tf.lite.Optimize.DEFAULT]
                converter.representative_dataset = representative_data_gen
                tflite_quant_model = converter.convert()
                
            elif quantizer_type.lower() == TensorflowQuantizerType.Float16Quantizer.value.lower():
                converter.optimizations = [tf.lite.Optimize.DEFAULT]
                converter.target_spec.supported_types = [tf.float16]
                converter.representative_dataset = representative_data_gen
                tflite_quant_model = converter.convert()

            return tflite_quant_model